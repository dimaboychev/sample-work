<?php

class EventsforceImport {
  private $event_node_type = 'eventsforce_event';
  private $conference_node_type = 'eventsforce_conference';
  private $eventsforce_conference_type = 'Residential conference';
  private $count = 0;
  private $ef_event_type_custom_field_id = 7;
  private $event_type_tids = array(
    'Seminar' => 2120,
    'Residential conference' => 2121,
    'Sessional programme' => 2122,
    'Masterclass' => 2123,
    'Open forum' => 2124,
    'Networking' => 2125,
    'Regional' => 2126,
    'Other organisations' => 2127,
    'External organisations' => 2128,
  );

  function __construct($event_type = 'live') {
    
    // Get the events from eventsforce.
    $this->get($event_type);

    // If some events are found, import them into Drupal.
    if ($this->events) {
      $this->import();
    }
  }

  /**
   * Get the events from eventsforce.
   *
   * @param string $event_type
   *   The type of event (e.g. live, notlive, archived etc).
   */
  function get($event_type) {
    $this->events = eventsforce_api_get_all_events($event_type);
  }

  /**
   * Import the events into Drupal nodes.
   */
  private function import() {
    foreach ($this->events as $event) {
      // Get data for this event.
      $event_data = eventsforce_api_get_event($event->eventID);

      // Does this event already exist within Drupal?
      $nid = self::eventExist($event, $event_data);

      // Create a new node or update an existing one.
      if (!$nid) {
        self::create($event, $event_data);
      }
      else {
        if (variable_get('eventsforce_import_update_existing', 1)) {
          self::update((int) $nid, $event, $event_data);
        }
      }

      $this->count++;
    }
  }

  /**
   * Returns the type of an event.
   */
  private function eventType($event_data) {
    $eventsforce_type = eventsforce_api_get_custom_event_field($this->ef_event_type_custom_field_id, $event_data);
    $event_type = $this->event_node_type;
    if ($eventsforce_type == $this->eventsforce_conference_type) {
      $event_type = $this->conference_node_type;
    }
    return $event_type;
  }

  /**
   * Check if an event already exists as a node.
   */
  private function eventExist($event, $event_data) {
    // Get the event type.
    $node_type = $this->eventType($event_data);

    // Search for an eventsforce_event node with this eventsforce ID.
    return db_select('field_data_eventsforce_id', 'id')
      ->fields('id', array('entity_id'))
      ->condition('bundle', $node_type)
      ->condition('eventsforce_id_value', $event->eventID)
      ->execute()
      ->fetchField();
  }

  /**
   * Create a new node.
   */
  private function create($event, $event_data) {
    // Get the node type for this event.
    $node_type = $this->eventType($event_data);

    // Create a new node within Drupal.
    $entity = entity_create('node', array('type' => $node_type));
    $wrapper = entity_metadata_wrapper('node', $entity);

    // Mirror the eventsforce data to fields.
    $wrapper->title->set($event->eventName);
    $wrapper->eventsforce_url->set($event->detailsURL);
    $wrapper->eventsforce_id->set($event->eventID);
    $wrapper->eventsforce_status->set($event->eventStatus);
    $wrapper->eventsforce_date->set(array(
      'value' => strtotime($event->eventStartDateTime),
      'value2' => strtotime($event->eventEndDateTime)
    ));
//    $wrapper->eventsforce_venue->set($event->venueName);

    // Get the eventsforce type.
    $eventsforce_type = eventsforce_api_get_custom_event_field($this->ef_event_type_custom_field_id, $event_data);
    if ($node_type == 'eventsforce_event' && isset($this->event_type_tids[$eventsforce_type])) {
      // Set the type.
      $event_type_tid = $this->event_type_tids[$eventsforce_type];
      $wrapper->field_event_type->set($event_type_tid);
    }

    // Save the node.
    $wrapper->save();
  }

  /**
   * Update a node.
   *
   * @param integer $nid
   *   The nid of the existing node.
   *
   * @param object $event
   *   The event from eventsforce.
   */
  private function update($nid, $event, $event_data) {
    $node = node_load($nid);
    $wrapper = entity_metadata_wrapper('node', $node);

    // Update any details. Some details such as URL and ID will not be updated.
    $wrapper->title->set($event->eventName);
    $wrapper->eventsforce_status->set($event->eventStatus);
    $wrapper->eventsforce_date->set(array(
      'value' => strtotime($event->eventStartDateTime),
      'value2' => strtotime($event->eventEndDateTime)
    ));
//    $wrapper->eventsforce_venue->set($event->venueName);

    // Get the node type for this event.
    $node_type = $node->type;
    // Get the eventsforce type.
    $eventsforce_type = eventsforce_api_get_custom_event_field($this->ef_event_type_custom_field_id, $event_data);
    if ($node_type == 'eventsforce_event' && isset($this->event_type_tids[$eventsforce_type])) {
      // Set the type.
      $event_type_tid = $this->event_type_tids[$eventsforce_type];
      $wrapper->field_event_type->set($event_type_tid);
    }

    // Update the node.
    $wrapper->save();
  }
}
