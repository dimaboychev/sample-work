<?php

/**
 * @file
 *
 */

/**
 * Form constructor for the settings form.
 *
 * @see eventsforce_api_admin_form_validate()
 * @see eventsforce_api_admin_form_submit()
 *
 * @ingroup forms
 */
function eventsforce_api_admin_form($form, $form_state) {
  $form['eventsforce_api'] = array(
    '#title' => t('API settings'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
  );

  $form['eventsforce_api']['eventsforce_api_account_name'] = array(
    '#title' => t('Account name'),
    '#description' => t('The customer account name that you use to log in to eventsforce.'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('eventsforce_api_account_name'),
  );

  $form['eventsforce_api']['eventsforce_api_key'] = array(
    '#title' => t('API key'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('eventsforce_api_key'),
  );

  return system_settings_form($form);
}

/**
 * Form validation handler for eventsforce_admin_form().
 */
function eventsforce_api_admin_form_validate($form, $form_state) {
  // @todo: Check that the credentials are correct and that a connection can be made to the API.
}
