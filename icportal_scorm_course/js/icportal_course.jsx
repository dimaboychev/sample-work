var CourseExpBox = React.createClass({
    loadCourseExp: function loadCourseExp() {
        var initialData = new Array();
        initialData['completion'] = 'Loading...';
        initialData['last_access'] = 'Loading...';
        initialData['status'] = 'Loading...';
        initialData['action'] = 'Loading...';
        this.setState({ courseExpData: initialData });

        $.ajax({
            url: this.props.url,
            dataType: 'json',
            cache: false,
            success: (function (data) {
                this.setState({ courseExpData: data });
                return true;
            }).bind(this),
            error: (function (xhr, status, err) {
                console.error(this.props.url, status, err.toString());
                return false;
            }).bind(this)
        });
    },
    getInitialState: function getInitialState() {
        return { courseExpData:  []};
    },
    componentDidMount: function componentDidMount() {
        //this.loadCourseStatus();
        //setInterval(this.loadCourseStatus, this.props.pollInterval);
    },
    componentWillMount : function componentWillMount() {
        this.loadCourseExp();
    },
    render: function() {
        return (
            <div>
                <div className="panel-body ic-panel-body">
                    <CourseExpCompletion courseCompletionData={this.state.courseExpData} />
                    <CourseExpOthers courseOtherData={this.state.courseExpData} />
                </div>
                <div className="panel-footer ic-panel-footer">
                    <CourseAction courseActionData={this.state.courseExpData} />
                </div>
            </div>
        );
    }
});

var CourseExpCompletion = React.createClass({
    getInitialState: function getInitialState() {
        var initialData = new Array();
        initialData['course'] = '';
        initialData['learner'] = '';
        initialData['score'] = '';
        initialData['completion'] = 'Loading...';
        initialData['completion_classes'] = '';
        initialData['last_access'] = '';
        initialData['finished_time'] = '';
        initialData['status'] = '';
        initialData['action'] = '';
        return { courseCompletionData:  initialData};
    },
    componentWillReceiveProps: function (nextProps) {
        this.setState({ courseCompletionData: nextProps.courseCompletionData });
    },
    render: function() {
        return (
            <div className={this.state.courseCompletionData.completion_classes}>{this.state.courseCompletionData.completion}</div>
        );
    }
});

var CourseExpOthers = React.createClass({
    getInitialState: function getInitialState() {
        var initialData = new Array();
        initialData['course'] = 'Loading...';
        initialData['learner'] = 'Loading...';
        initialData['score'] = '';
        initialData['completion'] = 'Loading...';
        initialData['completion_classes'] = 'Loading...';
        initialData['last_access'] = 'Loading...';
        initialData['finished_time'] = '';
        initialData['status'] = 'Loading...';
        initialData['action'] = 'Loading...';
        return { courseExpData:  initialData};
    },
    componentWillReceiveProps: function (nextProps) {
        this.setState({ courseExpData: nextProps.courseOtherData });
    },
    render: function() {
        return (
            <ul className="icportal-course-status">
                {
                this.state.courseExpData.score &&
                <li>
                    <div className="icportal-course-status-label">Score:</div>
                    <div className="icportal-course-status-value">{this.state.courseExpData.score}</div>
                </li>
                }
                {
                this.state.courseExpData.finished_time &&
                <li>
                    <div className="icportal-course-status-label">Finished Time:</div>
                    <div className="icportal-course-status-value">{this.state.courseExpData.finished_time}</div>
                </li>
                }
                <li>
                    <div className="icportal-course-status-label">Last Access:</div>
                    <div className="icportal-course-status-value">{this.state.courseExpData.last_access}</div>
                </li>
                <li>
                    <div className="icportal-course-status-label">Course Status:</div>
                    <div className="icportal-course-status-value">{this.state.courseExpData.status}</div>
                </li>
            </ul>
        );
    }
});

var CourseAction = React.createClass({
    clickHandler: function(e) {
        if (this.state.courseExpData.action == 'Start Over') {
            //Drupal.settings.icportal_scorm.info.mode = 'review';
            $.ajax({
                url: this.props.url,
                dataType: 'json',
                cache: false,
                success: (function (data) {
                    this.setState({ courseExpData: data });
                    return true;
                }).bind(this),
                error: (function (xhr, status, err) {
                    console.error(this.props.url, status, err.toString());
                    return false;
                }).bind(this)
            });
        }
        if (this.state.courseExpData.action == 'Re-Start') {
        }
        ReactDOM.render(
            <CoursePlayer />,
            document.getElementById('icportal-scorm-player')
        );
        var bodyHeight = $('body').height();
        var bodyWidth = $('body').width();

        var navbarHeight = $('#navbar').height();

        var iframeHeight = $('#frame-here').height((bodyHeight - navbarHeight));
        var iframeWidth = $('#frame-here').width(bodyWidth);

        $( window ).on("resize", function() {
            bodyHeight = $('body').height();
            bodyWidth = $('body').width();

            iframeHeight = $('#frame-here').height((bodyHeight - navbarHeight));
            iframeWidth = $('#frame-here').width(bodyWidth);
        });
        $('#icportal-course-info').hide();
        courseExitRef.setState({ hidden: false });
        if (this.state.courseExpData.action == 'Start Over') {

        }
        if (this.state.courseExpData.action == 'Re-Start') {
        }
    },
    getInitialState: function getInitialState() {
        var initialData = new Array();
        initialData['course'] = '';
        initialData['learner'] = '';
        initialData['score'] = '';
        initialData['completion'] = '';
        initialData['completion_classes'] = '';
        initialData['last_access'] = '';
        initialData['finished_time'] = '';
        initialData['status'] = '';
        initialData['action'] = 'Loading...';
        return { courseExpData:  initialData};
    },
    componentWillReceiveProps: function (nextProps) {
        this.setState({ courseExpData: nextProps.courseActionData });
    },
    render: function() {
        return (
            <button id="icportal-course-launch" className="btn btn-primary" onClick={this.clickHandler}>{this.state.courseExpData.action}</button>
        );
    }
});

var CourseExit = React.createClass({
    clickHandler: function(e) {
        $(window).unbind('beforeunload.icportal');
        window.location.reload();
        /*
         if (API) {
         API.LMSCommit('');
         API.LMSFinish('');
         } else if (API_1484_11) {
         API_1484_11.Commit('');
         API_1484_11.Terminate('');
         }
         ReactDOM.unmountComponentAtNode(document.getElementById('icportal-scorm-player'));
         //$('#icportal-scorm-player').hide();
         courseExpBoxRef.loadCourseExp();
         $('#icportal-course-info').show();
         this.setState({ hidden: true });
         */
    },
    getInitialState: function getInitialState() {
        return { hidden: true};
    },
    render: function() {
        return (
            <a id="icportal-course-exit-link" className={this.state.hidden ? 'hidden' : ''} role="button" onClick={this.clickHandler}>Exit</a>
        );
    }
});

var CoursePlayer = React.createClass({
    componentDidMount: function componentDidMount() {
        prepareFrames();
    },
    render: function() {
        return (
            <div className="icportal-scorm-player">
                <div className="jca-navigation-menu">
                    <p className="menu-helper">
                        <span id="show_statement_log_holder" style={{visibility: 'hidden'}}> |
                            <a id="show_statement_holder" href="#" onclick="showStatementLog()">Show Statement Log</a>
                        </span>
                    </p>
                    <div id="sco_navigation">
                        <button id="exit" onclick="navigate('exit')">Exit</button>
                        <button id="prev" onclick="navigate('previous')">Previous</button>
                        <button id="next" onclick="navigate('next')">Next</button>
                    </div>
                </div>
                <div className="jca-toc-holder">
                    <div id="toc">
                    </div>
                    <div style={{display: 'none'}}>
                        <form name="frmMain" id="frmMain" action>
                            <br />
                            <label htmlFor="student_name">Learner Name:</label> <input name="student_name" id="student_name" defaultValue type="text" />
                            <br />
                            <label htmlFor="student_id">Learner ID:</label> <input name="student_id" id="student_id" defaultValue type="text" />
                            <br />
                            <label htmlFor="credit-credit">Launch the SCOs in this course for (credit/no-credit):</label>
                            <br />
                            <input name="credit" id="credit-credit" defaultValue="credit" type="radio" defaultChecked />Credit
                            <input name="credit" id="credit-no-credit" defaultValue="no-credit" type="radio" />No Credit
                            <br />
                            <label htmlFor="mode-normal">Launch the SCOs in (normal, browse, review) mode:</label>
                            <br />
                            <input name="mode" id="mode-normal" defaultValue="normal" type="radio" defaultChecked />Normal
                            <input name="mode" id="mode-browse" defaultValue="browse" type="radio" />Browse
                            <input name="mode" id="mode-review" defaultValue="review" type="radio" />Review
                        </form>
                        <br />
                    Note: For the ADL test suite Mary Learner should have no-credit and browsed set. Also, remember to clear
                    the cookies
                    between the
                    Joe Student and Mary Learner tests.
                        <br />
                        <div id="course_completion_status_div">Course Completion Status:
                            <div id="course_completion_status">incomplete</div>
                        </div>
                        <br />
                        <div id="course_success_status_div">Course Success Status:
                            <div id="course_success_status">failed</div>
                        </div>
                        <br />
                        <div id="course_grade_div">Course Grade:
                            <div id="course_grade">0</div>
                        </div>
                    </div>
                </div>
                <div id="frame-here">
                </div>
            </div>
        );
    }
});

var StartOverConfirmModal = React.createClass({
    clickHandler: function clickHandler(e) {
        $('ic-modal-so').modal('hide');
        var expData = { action: "restart"};
        $.ajax({
            url: this.props.setExpUrl,
            method: "POST",
            dataType: 'json',
            cache: false,
            data: expData,
            success: (function (data) {
                ReactDOM.render(React.createElement(CoursePlayer, null), document.getElementById('icportal-scorm-player'));
                var bodyHeight = $('body').height();
                var bodyWidth = $('body').width();

                var navbarHeight = $('#navbar').height();

                var iframeHeight = $('#frame-here').height((bodyHeight - navbarHeight));
                var iframeWidth = $('#frame-here').width(bodyWidth);
                return true;
            }).bind(this),
            error: (function (xhr, status, err) {
                console.error(this.props.setExpUrl, status, err.toString());
                return false;
            }).bind(this)
        });
    },
    render: function() {
        return (
            <div className="modal fade" id="ic-modal-so" tabIndex={-1} role="dialog" aria-labelledby="ic-modal-so-label">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">�</span></button>
                            <h4 className="modal-title" id="ic-modal-so-label">Confirmation</h4>
                        </div>
                        <div className="modal-body">
                        You have already completed/passed this training. Do you really want to start over? This will reset your training record.
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-primary" onClick={this.clickHandler}>Confirm</button>
                            <button type="button" className="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

var CourseExitLoader = React.createClass({
    render: function() {
        return (
            <div className="showbox">
                <div className="loader">
                    <img className="loading-img" src="/sites/all/modules/icportal_scorm/images/loading-spinning-bubbles.svg" alt="Exiting icon" />
                    <h1 className="loading-text">Exiting and Saving Training Data...</h1>
                </div>
            </div>
        );
    }
});

ReactDOM.render(
    <CourseExpBox url='' />,
    document.getElementById('icportal-course-exp')
);

ReactDOM.render(
    <CourseExit />,
    document.getElementById('icportal-course-exit')
);
