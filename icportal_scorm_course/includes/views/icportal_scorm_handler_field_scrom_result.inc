<?php

/**
 * @file
 * Definition of views_handler_field_contextual_links.
 */

/**
 * Provides a handler that output generalized SCORM result.
 * e.g. SCORM 2004 has completion_status (completed/incomplete) 
 * and success status (passed/failed)
 * 
 * @ingroup views_field_handlers
 */
class icportal_scorm_handler_field_scorm_result extends views_handler_field {

  /**
   * Render the SCORM result fields.
   */
  function render($values) {
    // Get value of completion_status
    $completion_status = $values->icportal_scorm_2004_parsed_completion_status;
    // Get value of success_status
    $success_status = $values->icportal_scorm_2004_parsed_success_status;

    if (!empty($completion_status)) {
      if ($completion_status == 'incomplete') {
        $generalised_completion_status = 'In Progress';
      }
      if ($completion_status == 'completed' && $success_status == 'passed') {
        $generalised_completion_status = 'Passed';
      }
      if ($completion_status == 'completed' && $success_status == 'failed') {
        $generalised_completion_status = 'Failed';
      }
      if ($completion_status == 'completed' && $success_status != 'failed' && $success_status != 'passed') {
        $generalised_completion_status = 'Completed';
      }
    }
    else {
      $generalised_completion_status = 'N/A';
    }

    return $generalised_completion_status;
  }

  function query() { }
}
