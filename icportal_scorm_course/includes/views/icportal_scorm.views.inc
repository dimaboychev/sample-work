<?php

/**
 * Implement hook_views_data().
 */
function icportal_scorm_views_data() {
  $data = array();

  // The 'group' index will be used as a prefix in the UI for any of this
  // table's fields, sort criteria, etc. so it's easy to tell where they came
  // from.
  $data['icportal_scorm_course']['table']['group'] = t('ICPortal Course');

    // Next, describe each of the individual fields in this table to Views. This
  // is done by describing $data['example_table']['FIELD_NAME']. This part of
  // the array may then have further entries:
  //   - title: The label for the table field, as presented in Views.
  //   - help: The description text for the table field.
  //   - relationship: A description of any relationship handler for the table
  //     field.
  //   - field: A description of any field handler for the table field.
  //   - sort: A description of any sort handler for the table field.
  //   - filter: A description of any filter handler for the table field.
  //   - argument: A description of any argument handler for the table field.
  //   - area: A description of any handler for adding content to header,
  //     footer or as no result behaviour.
  //
  // The handler descriptions are described with examples below.

  // Node ID table field.
  $data['icportal_scorm_course']['node_id'] = array(
    'title' => t('Course Node ID'),
    'help' => t('Course node ID that references a node.'),
    // Define a relationship to the {node} table, so example_table views can
    // add a relationship to nodes. If you want to define a relationship the
    // other direction, use hook_views_data_alter(), or use the 'implicit' join
    // method described above.
    'relationship' => array(
      'base' => 'node', // The name of the table to join with.
      'base field' => 'nid', // The name of the field on the joined table.
      // 'field' => 'nid' -- see hook_views_data_alter(); not needed here.
      'handler' => 'views_handler_relationship',
      'label' => t('Course Node ID'),
      'title' => t('Course Node ID'),
      'help' => t('More information on this relationship'),
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_node_nid',
    ),
  );

  // Example plain text field.
  $data['icportal_scorm_course']['scorm_version'] = array(
    'title' => t('Course SCORM version'),
    'help' => t('SCORM version'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is use by the table display plugin.
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  $data['icportal_scorm_course']['id'] = array(
      'title' => t('Course ID'),
      'help' => t('Course ID'),
      'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE, // This is use by the table display plugin.
      ),
      'sort' => array(
          'handler' => 'views_handler_sort',
      ),
      'filter' => array(
          'handler' => 'views_handler_filter_string',
      ),
  );


  // Table: icportal_scorm_course_assignment
  $data['icportal_scorm_course_assignment']['table']['group'] = t('ICPortal Course Assignment');

  $data['icportal_scorm_course_assignment']['table']['join'] = array(
    'users' => array(
      'left_field' => 'uid',
      'field' => 'learner_id',
    ),
  );

  $data['icportal_scorm_course_assignment']['course_id'] = array(
    'title' => t('Course ID'),
    'help' => t('Course ID that references a course.'),
    'relationship' => array(
      'base' => 'icportal_scorm_course', // The name of the table to join with.
      'base field' => 'id', // The name of the field on the joined table.
      // 'field' => 'nid' -- see hook_views_data_alter(); not needed here.
      'handler' => 'views_handler_relationship',
      'label' => t('Course ID'),
      'title' => t('Course ID'),
      'help' => t('More information on this relationship'),
    ),
  );

  $data['icportal_scorm_course_assignment']['created'] = array(
    'title' => t('Course Assignment Created TimeStamp'),
    'help' => t('Course Assignment Created TimeStamp'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE, // This is use by the table display plugin.
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  // Table: icportal_scorm_2004_parsed
  $data['icportal_scorm_2004_parsed']['table']['group'] = t('ICPortal Course Results');

  $data['icportal_scorm_2004_parsed']['table']['join'] = array(
    'users' => array(
      'left_field' => 'uid',
      'field' => 'learner_id',
    ),
  );

  $data['icportal_scorm_2004_parsed']['course_id'] = array(
    'title' => t('Course ID'),
    'help' => t('Course ID that references a course.'),
    'relationship' => array(
      'base' => 'icportal_scorm_course', // The name of the table to join with.
      'base field' => 'id', // The name of the field on the joined table.
      // 'field' => 'nid' -- see hook_views_data_alter(); not needed here.
      'handler' => 'views_handler_relationship',
      'label' => t('Course ID'),
      'title' => t('Course ID'),
      'help' => t('More information on this relationship'),
    ),
  );

  $data['icportal_scorm_2004_parsed']['completion_status'] = array(
    'title' => t('Course Completion'),
    'help' => t('Course Completion Status'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is use by the table display plugin.
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_in_operator',
      'options callback' => 'icportal_scorm_completion_status_list',
      'options arguments' => array(),
    ),
  );

  $data['icportal_scorm_2004_parsed']['success_status'] = array(
    'title' => t('Course Success'),
    'help' => t('Course Success Status'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is use by the table display plugin.
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_in_operator',
      'options callback' => 'icportal_scorm_success_status_list',
      'options arguments' => array(),
    ),
  );

  $data['icportal_scorm_2004_parsed']['result'] = array(
    'title' => t('Course Result'),
    'help' => t('Course Result'),
    'field' => array(
      'handler' => 'icportal_scorm_handler_field_scorm_result',
      'click sortable' => TRUE, // This is use by the table display plugin.
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_in_operator',
      'options callback' => 'icportal_scorm_result_options',
      'options arguments' => array(),
    ),
  );

  $data['icportal_scorm_2004_parsed']['access'] = array(
    'title' => t('Course Last Access Timestamp'),
    'help' => t('Course Last Access Timestamp'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE, // This is use by the table display plugin.
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['icportal_scorm_2004_parsed']['finish'] = array(
    'title' => t('Course Finished Timestamp'),
    'help' => t('Course Finished Timestamp'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE, // This is use by the table display plugin.
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  return $data;
}

/**
 * Implements hook_views_data_alter()
 * @param $data
 */

function icportal_scorm_views_data_alter(&$data) {

}

/**
 * Implements hook_views_query_alter()
 */
function icportal_scorm_views_query_alter(&$view, &$query) {
  if ($view->name == 'testing_2') {
    //dpm($view);
    //dpm($query);
  }
}

/**
 * Callback function for views_handler_filter_in_operator
 */
function icportal_scorm_completion_status_list() {
  $options = array(
    t('Completed') => 'completed',
    t('Incomplete') => 'incomplete',
    t('Unknown') => 'unknown',
  );

  return $options;
}

/**
 * Callback function for views_handler_filter_in_operator
 */
function icportal_scorm_success_status_list() {
  $options = array(
    t('Passed') => 'passed',
    t('Failed') => 'failed',
    t('Unknown') => 'unknown',
  );

  return $options;
}

/**
 * Callback function for views_handler_filter_in_operator
 */
function icportal_scorm_result_options() {
  $options = array(
    t('Passed') => 'Passed',
    t('Completed') => 'Completed',
    t('In Progress') => 'In Progress',
    t('Failed') => 'Failed',
    t('Unknown') => 'N/A',
  );

  return $options;
}