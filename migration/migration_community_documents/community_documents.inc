<?php

class CommunityDocumentsMigration extends Migration {

    public function __construct($arguments) {
        parent::__construct($arguments);
        $query = Database::getConnection('default', 'migrate')->select('node', 'n');
        $query->join('field_data_field_community_document_type', 'cdt', 'cdt.entity_id = n.nid');
        $query->fields('n', array('nid', 'title'));
        $query->fields('cdt', array('entity_type', 'bundle'));
                
        //->fields('cdt', array('entity_type', 'bundle', 'deleted', 'entity_id', 'revision_id', 'language', 'delta', 'field_community_document_type_target_id'))
        //$query->join('field_data_field_community_document_type', 'cdt', 'cdt.entity_id = n.nid');
        //$query->addField('cdt', 'entity_id');

        $this->source = new MigrateSourceSQL($query);
        $this->destination = new MigrateDestinationNode('community_document');
        $sourceKey = array(
            'nid' => array(
                'type' => 'int',
                'not null' => TRUE,
            ),
        );
        $destKey = MigrateDestinationNode::getKeySchema();
        $this->map = new MigrateSQLMap($this->machineName, $sourceKey, $destKey);
        $this->addFieldMapping('title', 'title');
        $this->addFieldMapping('field_community_document_type', 'bundle');
        // $this->addFieldMapping('uid')->defaultValue(1)->description('Make admin the author of migrated content');
        // $this->addFieldMapping('created')->description('Use current timestamp');
        // $this->addFieldMapping('field_date', 'date');
        // $this->addFieldMapping('field_date:timezone', 'tz');
        // $this->addFieldMapping('field_venue', 'venue_url');
        // $this->addFieldMapping('field_venue:title', 'venue_text');
        // $this->addFieldMapping('body', 'description');
        // $this->addFieldMapping('body:format')->defaultValue('plain_text');
    }

    // public function prepare($node, stdClass $row) {
    // if(!empty($row->tz)) {
    // The 'field_date:timezone' mapping does not seem to work for some unknown reason.
    // So we have to do this.
    // $node->field_date[LANGUAGE_NONE][0]['timezone'] = $row->tz;
    // }
    // }
}
