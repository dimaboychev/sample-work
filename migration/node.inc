<?php

/**
 * Common mappings for the Drupal 6 node migrations.
 */
abstract class IFOANodeMigration extends DrupalNode6Migration {
  public function __construct(array $arguments) {
    parent::__construct($arguments);
  }
}

class DocumentMigration extends IFOANodeMigration {
  public function __construct(array $arguments) {
    $arguments['dependencies'] = array(
      'DocumentType',
      'EventAccess',
      'FunctionalArea',
      'PracticeAreas',
    );

    parent::__construct($arguments);

    $this->addSimpleMappings(array(
      'field_document_author',
      'field_document_source',
    ));

    $this->addFieldMapping('field_document_type', 28)
         ->sourceMigration('DocumentType')
         ->arguments(array('source_type' => 'tid'));

    $this->addFieldMapping('field_event_access', 36)
         ->sourceMigration('EventAccess')
         ->arguments(array('source_type' => 'tid'));

    $this->addFieldMapping('field_functional_area', 29)
         ->sourceMigration('FunctionalArea')
         ->arguments(array('source_type' => 'tid'));

    $this->addFieldMapping('field_practice_area', 2)
         ->sourceMigration('PracticeAreas')
         ->arguments(array('source_type' => 'tid'));
  }
}

/**
 * Common content mappings for the IFOA D6 migration.
 */
class IFOAContentNodeMigration extends IFOANodeMigration {
  public function __construct(array $arguments) {

    parent::__construct($arguments);

    // Set simple mappings for some standard fields.
    $this->addSimpleMappings(array(
      'title',
      'body',
      'uid',
      'created',
    ));

    // Map the body filter format.
    $this->addFieldMapping('body:format', 'format')
      // This matches the D6 id's to the D7 machine names.
      ->callbacks('ifoa_migrate_filter_mapping')
      // Default to Filtered HTML.
      ->defaultValue('filtered_html');

    // Map the summary field.
    $this->addFieldMapping('body:summary', 'teaser');
  }
}

/**
 * Migration handler for Press Releases.
 */
class PressReleaseMigration extends IFOAContentNodeMigration {
  public function __construct(array $arguments) {

    $arguments['dependencies'] = array(
      'SiteArea',
      'PracticeAreas',
    );
    parent::__construct($arguments);

    // Use the article date field
    $this->addFieldMapping('created', 'field_article_date')
      // We're converting a field value [array(0 => 'value')] to a single entity
      // attribute ['value'], so we need to grab the first array item.
      ->callbacks('ifoa_migrate_field_attribute');

    // The D6 site uses a separate summary field.
    $this->addFieldMapping('field_intro', 'field_summary')
      // This is a plain text field in D7, so remove all HTML.
      ->callbacks('ifoa_migrate_strip_tags');

    // The article image.
    $this->addFieldMapping('field_image', 'field_picture')
      // Use the Document/file migration to map the fid's.
      ->sourceMigration('InstituteandFacultyofActuariesFile');
    // The files have already beeen migrated, so it's a straight fid migration.
    $this->addFieldMapping('field_image:file_class')
      ->defaultValue('MigrateFileFid');
    $this->addFieldMapping('field_image:preserve_files')
      ->defaultValue(TRUE);

    // Taxonomy migration
    $this->addFieldMapping('field_practice_area', 2)
      ->sourceMigration('PracticeAreas')
      ->arguments(array('source_type' => 'tid'));
    $this->addFieldMapping('field_site_area', 11)
      ->sourceMigration('SiteArea')
      ->arguments(array('source_type' => 'tid'));

  }
}

/**
 * Migration handler for News articles.
 */
class NewsMigration extends IFOAContentNodeMigration {
  public function __construct(array $arguments) {

    $arguments['dependencies'] = array(
      'PracticeAreas',
    );
    parent::__construct($arguments);

    // Use the article date field
    $this->addFieldMapping('created', 'field_article_date')
      // We're converting a field value [array('value')] to a single entity
      // attribute ['value'], so we need to grab the first array item.
      ->callbacks('ifoa_migrate_field_attribute');

    // The article image.
    $this->addFieldMapping('field_image', 'field_picture')
      // Use the Document/file migration to map the fid's.
      ->sourceMigration('InstituteandFacultyofActuariesFile');
      // The files have already beeen migrated, so it's a straight fid migration.
    $this->addFieldMapping('field_image:file_class')
      ->defaultValue('MigrateFileFid');
    $this->addFieldMapping('field_image:preserve_files')
      ->defaultValue(TRUE);

    // Taxonomy migration
    $this->addFieldMapping('field_practice_area', 2)
      ->sourceMigration('PracticeAreas')
      ->arguments(array('source_type' => 'tid'));

  }
}

/**
 * Migration handler for Volunteer Opportunity content.
 */
class VolunteerOpportunityMigration extends IFOAContentNodeMigration {
  public function __construct(array $arguments) {

    $arguments['dependencies'] = array(
      'PracticeAreas',
    );
    parent::__construct($arguments);

    // Closing date.
    $this->addFieldMapping('field_date', 'field_volopp_closingdate');

    // Tenure field.
    $this->addFieldMapping('field_tenure', 'field_volopp_tenure');

    // Taxonomy migration
    $this->addFieldMapping('field_practice_area', 2)
      ->sourceMigration('PracticeAreas')
      ->arguments(array('source_type' => 'tid'));

  }
}

/**
 * Migration handler for Person content.
 */
class PersonMigration extends IFOAContentNodeMigration {
  public function __construct(array $arguments) {

    $arguments['dependencies'] = array(
      'PersonBio',
    );
    parent::__construct($arguments);

    // The person's image.
    $this->addFieldMapping('field_person_photo', 'field_plenary_picture')
      // Use the Document/file migration to map the fid's.
      ->sourceMigration('InstituteandFacultyofActuariesFile');

    // The files have already beeen migrated, so it's a straight fid migration.
    /*
    // Photos are not required.
    $this->addFieldMapping('field_person_photo:file_class')
      ->defaultValue('MigrateFileFid');
    $this->addFieldMapping('field_person_photo:preserve_files')
      ->defaultValue(TRUE);
    // */

    // Residential events aren't required.
    // $this->addFieldMapping('field_related_nodes', 'field_residential_events');

    // Taxonomy migration
    $this->addFieldMapping('field_person_type', 38)
      ->sourceMigration('PersonBio')
      ->arguments(array('source_type' => 'tid'));

  }
}

/**
 * Migration handler for Actuarial Firm content.
 */
class ActuarialFirmMigration extends IFOAContentNodeMigration {
  public function __construct(array $arguments) {

    $arguments['dependencies'] = array(
      'Region',
      'FirmType',
      'FirmAdviceType',
    );

    parent::__construct($arguments);

    $this->addSimpleMappings(array(
      'field_firm_display_name',
      'field_firm_index',
    ));

    // The Actuary logo.
    $this->addFieldMapping('field_firm_logo', 'field_firm_logo')
      // Use the Document/file migration to map the fid's.
      ->sourceMigration('InstituteandFacultyofActuariesFile');
    // The files have already beeen migrated, so it's a straight fid migration.
    $this->addFieldMapping('field_firm_logo:file_class')
      ->defaultValue('MigrateFileFid');
    $this->addFieldMapping('field_firm_logo:preserve_files')
      ->defaultValue(TRUE);

    // Taxonomy migration
    $this->addFieldMapping('field_firm_type', 'field_firm_type')
      ->sourceMigration('FirmType')
      ->arguments(array('source_type' => 'tid'));

    $this->addFieldMapping('field_firm_region', 'field_firm_region')
      ->sourceMigration('Region')
      ->arguments(array('source_type' => 'tid'));

    $this->addFieldMapping('field_firm_advice_type', 'field_firm_advice')
      ->sourceMigration('FirmAdviceType')
      ->arguments(array('source_type' => 'tid'));

    // @todo Migrate the field_firm_offices Field collection.
  }
}

/**
 * Migration handler for Office Field Collections attached to Actuarial Firms.
 */
class ActuarialFirmFieldCollectionMigration extends IFOANodeMigration {
  public function __construct(array $arguments) {

    // The Field Collection must be migrated after the main node migration.
    $arguments['dependencies'] = array(
      'ActuarialFirm',
    );

    parent::__construct($arguments);

    $this->setDestination(new MigrateDestinationFieldCollection(
      'field_firm_offices',
      array('host_entity_type' => 'node')
    ));

    // Match the D6 nid to the imported Actuarial Firm, and attach the field
    // collection to it.
    $this->addFieldMapping('host_entity_id', 'nid')
      ->sourceMigration('ActuarialFirm');

    // The fields to include in the Field Collection.
    $this->addFieldMapping('field_office_name', 'field_firm_display_name');
    $this->addFieldMapping('field_office_number_of_actuaries', 'field_firm_number');
    $this->addFieldMapping('field_office_email', 'field_firm_email');

  }
}

/**
 * Migration handler for Group content.
 */
class GroupMigration extends IFOAContentNodeMigration {
  public function __construct(array $arguments) {
  
    $arguments['dependencies'] = array(
      'FunctionalArea',
      'SiteArea',
    );
    
    parent::__construct($arguments);
    
    // Amend the source query to include OG.
    $query = $this->query();
    $query->join('og', 'og', 'n.nid = og.nid');
    // Retrieve the group visibility value.
    $query->fields('og', array('og_private'));
    $this->sourceOptions['fix_field_names'] = $this->fixFieldNames;
    $this->source = new MigrateDrupal6SourceSQL($query, $this->sourceFields, NULL, $this->sourceOptions);
    
    // Map the group visibility field (defaults to private).
    $this->addFieldMapping('group_access', 'og_private')->defaultValue(1);
    // All migrated community nodes are OG groups.
    $this->addFieldMapping('group_group')->defaultValue(1);
    
    // The group owner MUST be a valid authenticated user.
    $this->addFieldMapping('uid', 'uid')
      ->callbacks('ifoa_migrate_valid_uid');
    
    $this->addSimpleMappings(array(
    	'field_community_exam',
      'field_exam_length',
      'field_exam_period',
      'field_exam_period:value2',
    ));
    $this->addFieldMapping('field_exam_message', 'field_exam_msg');
    
    // Map the exam paper files.
    $this->addFieldMapping('field_exam_papers', 'field_exam_paper')
      // Use the Document/file migration to map the fid's.
      ->sourceMigration('InstituteandFacultyofActuariesFile');
    // The files have already beeen migrated, so it's a straight fid migration.
    $this->addFieldMapping('field_exam_papers:file_class')
      ->defaultValue('MigrateFileFid');
    $this->addFieldMapping('field_exam_papers:preserve_files')
      ->defaultValue(TRUE);
    
    // Taxonomy mapping for functional area vocabulary.
    $this->addFieldMapping('field_functional_area', 29)
      ->sourceMigration('FunctionalArea')
      ->arguments(array('source_type' => 'tid'));

    // Taxonomy mapping for site area vocabulary. NOTE: The entity reference 
    // field is different to all other content types -
    // field_site_area_for_communities vs field_site_area.
    $this->addFieldMapping('field_site_area_for_communities', 11)
      ->sourceMigration('SiteArea')
      ->arguments(array('source_type' => 'tid'));
  }
}

/**
 * Migration handler for Forums.
 */
class ForumTopicMigration extends IFOAContentNodeMigration {
  public function __construct(array $arguments) {

    $arguments['dependencies'] = array(
      'Group',
    );
    parent::__construct($arguments);

    // Amend the source query to include OG Forum details.
    $query = $this->query();
    $query->join('term_node', 'f_tn', 'n.vid = f_tn.vid');
    $query->join('term_data', 'f_td', 'f_tn.tid = f_td.tid');
    $query->join('og_term', 'f_ogt', 'f_td.tid = f_ogt.tid');
    $query->join('node', 'f_group', 'f_ogt.nid = f_group.nid');
    // Retrieve the group nid.
    $query->addField('f_group', 'nid', 'group_nid');
    $this->sourceOptions['fix_field_names'] = $this->fixFieldNames;
    $this->source = new MigrateDrupal6SourceSQL($query, $this->sourceFields, NULL, $this->sourceOptions);
    
    // SELECT n.type, c.type, n.title, c.title, ogt.nid as community_nid FROM node n INNER JOIN term_node tn on n.vid = tn.vid
    // INNER JOIN term_data td ON tn.tid = td.tid INNER JOIN og_term ogt ON td.tid = ogt.tid  INNER JOIN node c ON ogt.nid = c.nid WHERE n.type = 'forum' AND td.vid = 6

    // Map the parent community.
    $this->addFieldMapping('field_og_forum', 'group_nid')
      // Use the Group/Community migration to map the gid's.
      ->sourceMigration('Group')
      ->callbacks('ifoa_migrate_community_forum_mapping');

  }
}

/**
 * Migration handler for Community Documents.
 */
class CommunityDocumentMigration extends IFOAContentNodeMigration {
  public function __construct(array $arguments) {

    $arguments['dependencies'] = array(
      'DocumentFormat',
      'CommunityDocumentType',
    );

    parent::__construct($arguments);

    $this->addSimpleMappings(array(
      'field_firm_display_name',
      'field_firm_index',
    ));

    // The Actuary logo.
    //$this->addFieldMapping('field_document_files', 'field_document_files')
      // Use the Document/file migration to map the fid's.
      //->sourceMigration('InstituteandFacultyofActuariesFile');
    // The files have already beeen migrated, so it's a straight fid migration.
    //$this->addFieldMapping('field_firm_logo:file_class')
      //->defaultValue('MigrateFileFid');
    //$this->addFieldMapping('field_firm_logo:preserve_files')
      //->defaultValue(TRUE);

    $this->addFieldMapping('field_document_files', 'field_document_file')
      ->sourceMigration('InstituteandFacultyofActuariesFile');
    $this->addFieldMapping('field_document_files:file_class')
      ->defaultValue('MigrateFileFid');
    $this->addFieldMapping('field_document_files:preserve_files')
      ->defaultValue(TRUE);

    // Taxonomy migration
    $this->addFieldMapping('field_document_format', 'field_document_format')
      ->sourceMigration('DocumentFormat')
      ->arguments(array('source_type' => 'tid'));

    $this->addFieldMapping('field_community_document_type', 'field_community_document_type')
      ->sourceMigration('CommunityDocumentType')
      ->arguments(array('source_type' => 'tid'));

    //$this->addFieldMapping('field_firm_advice_type', 'field_firm_advice')
      //->sourceMigration('FirmAdviceType')
      //->arguments(array('source_type' => 'tid'));

  }
}
